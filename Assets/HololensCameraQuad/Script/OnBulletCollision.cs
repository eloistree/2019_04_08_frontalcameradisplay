﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnBulletCollision : MonoBehaviour
{
    public Transform m_planeHit;
    public Material m_fadeMaterial;
    public Material m_finalMaterial;
    public float m_lifeLostByHit=0.3f;
    public GameObject m_holePrefab;
    public UnityEvent m_totalyBlind;

    private void Awake()
    {
        Color color = m_fadeMaterial.color;
        color.a = 0f;
        m_fadeMaterial.color = color;
        m_finalMaterial.color = color;

    }

    private void OnCollisionEnter(Collision collision)
    {
        Color color = m_fadeMaterial.color;
        color.a += m_lifeLostByHit;
        m_fadeMaterial.color = color;
        GameObject obj = Instantiate(m_holePrefab, collision.contacts[0].point, m_planeHit.rotation);
        obj.transform.parent = m_planeHit;

        if (color.a >= 0.8f) {
            color = m_finalMaterial.color;
            color.a += m_lifeLostByHit;
            m_finalMaterial.color = color;

            if (color.a >= 1f) {
                m_totalyBlind.Invoke();
            }
        }
    }
}
